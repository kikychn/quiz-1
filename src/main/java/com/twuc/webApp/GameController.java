package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class GameController {

    @PostMapping("/games")
    public ResponseEntity<Game> getGame() {
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("/games/{gameId}")
    public ResponseEntity<Game> getGameWithGameId(@PathVariable Integer gameId) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header("Location", "/api/games/" + gameId)
                .body(new Game(gameId));
    }

    @GetMapping("/games/{gameId}")
    public ResponseEntity<Game> getGameStatus(@PathVariable Integer gameId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(new Game(gameId));
    }

    @PatchMapping("/games/{gameId}")
    public ResponseEntity<GuessResult> getGuessNumberStatus(@PathVariable Integer gameId, @RequestBody Answer guessAnswerObject) {
        String trueAnswer = getGameWithGameId(gameId).getBody().getAnswer();
        String guessAnswer = guessAnswerObject.getAnswer();
        GuessResult guessResult = new GuessResult(trueAnswer, guessAnswer);
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(guessResult);
    }


}
