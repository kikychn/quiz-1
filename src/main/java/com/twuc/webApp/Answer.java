package com.twuc.webApp;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Answer {
    private String answer;

    public Answer() {
        Random random = new Random();
        List<String> randomNums = new ArrayList<>();
        for (int i = 0; i < 4; ++i) {
            randomNums.add(String.valueOf(random.nextInt(10)));
        }

        this.answer = String.join("", randomNums);
    }

    public Answer(String answer) {
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }
}
