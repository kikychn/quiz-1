package com.twuc.webApp;

public class Game {
    private Integer id;
    private String answer;

    public Game() {
    }

    public Game(Integer id) {
        this.id = id;
        Answer answerObject = new Answer();
        this.answer = answerObject.getAnswer();
    }

    public Integer getId() {
        return id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
