package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;

import java.util.Arrays;
import java.util.List;

public class GuessResult {
    private String hint;
    private boolean correct;

    public GuessResult() {
    }

    public GuessResult(String trueAnswer, String guessAnswer) {
        String[] trueAnswerNumberArray = trueAnswer.split("");
        String[] guessAnswerNumberArray = guessAnswer.split("");
        Integer countA = 0;
        Integer countB = 0;
        Integer countSame = 0;
        for (int i = 0; i<trueAnswerNumberArray.length;++i) {
                if (trueAnswerNumberArray[i].equals(guessAnswerNumberArray[i])) {
                    countA++;
            }
        }
        List<String> trueAnswerNumberList = Arrays.asList(trueAnswerNumberArray);
        for (String guessNumber : guessAnswerNumberArray) {
            if (trueAnswerNumberList.contains(guessNumber)) {
                countSame++;
            }
        }
        countB = countSame - countA;

        this.hint = countA.toString() + "A" + countB.toString() + "B";
        if (countA.equals(4)) {
            this.correct = true;
        }
        this.correct = false;
    }

    public String getHint() {
        return hint;
    }

    public boolean isCorrect() {
        return correct;
    }
}
