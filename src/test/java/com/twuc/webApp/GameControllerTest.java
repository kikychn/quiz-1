package com.twuc.webApp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
class GameControllerTest {

    @Autowired
    private TestRestTemplate template;

    @Test
    void should_create_game() {
        ResponseEntity<Game> entity = template.postForEntity("/api/games", null, Game.class);

        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
    }

    @Test
    void should_create_game_with_header() {
        ResponseEntity<Game> entity = template.postForEntity("/api/games/2", null, Game.class);

        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
        assertEquals("[/api/games/2]", entity.getHeaders().get("Location").toString());
        assertEquals("2", entity.getBody().getId().toString());
    }

    @Test
    void should_get_game_status() {
        ResponseEntity<Game> entity = template.getForEntity("/api/games/2", Game.class);

        ObjectMapper objectMapper = new ObjectMapper();

        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, entity.getHeaders().getContentType());
        assertEquals("2", entity.getBody().getId().toString());
        assertEquals(4, entity.getBody().getAnswer().length());
    }

//
//    @Test
//    void should_return_guess_number_result() {
//        ResponseEntity<GuessResult> entity = template.patchForObject("/api/games/2", null, GuessResult.class);
//    }

}
